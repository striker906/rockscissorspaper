import java.util.Random;
import java.util.Scanner;

public class RockScissorsPaper {
            public static void main(String[] args) {
                System.out.println("Welcome in the \"Rock\", \"Scisors\" ,\"Paper\"  game");
                System.out.println("Make your choise: \"Rock\", \"Scisors\" ,\"Paper\"");
                RoundsPlay();
            }
            public static void RoundsPlay() {
                int roundNumber=0, computerWins=0, playerWins=0,Draw=0;
                Scanner in=new Scanner (System.in);
                Random rand=new Random();
                System.out.println("Enter your choice");

                do {

                    String playerChoise=in.next();
                    if(playerChoise.equalsIgnoreCase("Rock")){
                        int computerChoise=rand.nextInt(3)+1;
                        switch(computerChoise) {
                            case 1:
                                System.out.println("Rock vs Rock,Draw");
                                roundNumber=roundNumber+1;
                                Draw=Draw+1;
                                System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");
                                break;
                            case 2:
                                System.out.println("Rock vs Scisors,playerWin");
                                roundNumber=roundNumber+1;
                                playerWins=playerWins+1;
                                System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");
                                break;
                            case 3:
                                System.out.println("Rock vs Paper,computerWin");
                                roundNumber=roundNumber+1;
                                computerWins=computerWins +1;
                                System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");
                                break;
                        }
                    }
                    if(playerChoise.equalsIgnoreCase("Scisors")) {
                        int computerChoise=rand.nextInt(3)+1;
                        switch(computerChoise) {
                            case 1:
                                System.out.println("Scisors vs Rock, computerWin");
                                roundNumber=roundNumber+1;
                                computerWins=computerWins+1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");

                                break;

                            case 2:
                                System.out.println("Scisors vs Scisors, Draw");
                                roundNumber=roundNumber+1;
                                Draw=Draw+1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");

                                break;

                            case 3:
                                System.out.println("Scisors vs Paper, playerWin");
                                roundNumber=roundNumber+1;
                                playerWins=playerWins+1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");

                                break;
                        }

                    }
                    if(playerChoise.equalsIgnoreCase("Paper")) {
                        int computerChoise=rand.nextInt(3)+1;
                        switch(computerChoise) {
                            case 1:
                                System.out.println("Paper vs Rock, playerWin");
                                roundNumber=roundNumber+1;
                                playerWins=playerWins+1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");
                                break;

                            case 2:
                                System.out.println("Paper vs Scisors, computerWins");
                                roundNumber=roundNumber+1;
                                computerWins=computerWins+1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");

                                break;

                            case 3:
                                System.out.println("Paper vs Paper, Draw");
                                roundNumber=roundNumber+1;
                                Draw=Draw +1;
                                System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                                System.out.println("RoundNumber="+ roundNumber);
                                System.out.println("Choose another choice");

                                break;

                        }
                    }
                }
                while(roundNumber<2);
                LastRound( playerWins, computerWins, roundNumber, Draw);


            }
            public static void AnotherPlay() {
                System.out.println("Play another round");
                Scanner in2=new Scanner (System.in);
                String answer;
                answer=in2.nextLine();
                if(answer.equalsIgnoreCase("Yes")){
                    RoundsPlay();
                }
                else {
                    System.out.println("End");
                }
            }
            public static void Winner(int playerWins, int computerWins) {
                if(playerWins>computerWins) {
                    System.out.println("Player win the game" );
                    System.out.println("End of the game");
                }
                else if(playerWins<computerWins) {
                    System.out.println("Computer win the game");
                    System.out.println("End of the game");
                }
                else if(playerWins==computerWins) {
                    System.out.println("Draw");
                    System.out.println("End of the game");
                }
                AnotherPlay();
            }
            public static void LastRound(int playerWins, int computerWins, int roundNumber, int Draw) {
                Scanner in=new Scanner (System.in);
                Random rand=new Random();
                System.out.println("Enter your choice");
                String playerChoise=in.next();
                if(playerChoise.equalsIgnoreCase("Rock")){
                    int computerChoise=rand.nextInt(3)+1;
                    switch(computerChoise) {
                        case 1:
                            System.out.println("Rock vs Rock,Draw");
                            roundNumber=roundNumber+1;
                            Draw=Draw+1;
                            System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                            System.out.println("RoundNumber="+ roundNumber);
                            break;
                        case 2:
                            System.out.println("Rock vs Scisors,playerWin");
                            roundNumber=roundNumber+1;
                            playerWins=playerWins+1;
                            System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                            System.out.println("RoundNumber="+ roundNumber);
                            break;
                        case 3:
                            System.out.println("Rock vs Paper,computerWin");
                            roundNumber=roundNumber+1;
                            computerWins=computerWins +1;
                            System.out.println("playerWins= " + playerWins+ ",computerWins= "+ computerWins +" draw=" + Draw);
                            System.out.println("RoundNumber="+ roundNumber);
                            break;
                    }
                }
                if(playerChoise.equalsIgnoreCase("Scisors")) {
                    int computerChoise=rand.nextInt(3)+1;
                    switch(computerChoise) {
                        case 1:
                            System.out.println("Scisors vs Rock, computerWin");
                            roundNumber=roundNumber+1;
                            computerWins=computerWins+1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);

                            break;

                        case 2:
                            System.out.println("Scisors vs Scisors, Draw");
                            roundNumber=roundNumber+1;
                            Draw=Draw+1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);

                            break;

                        case 3:
                            System.out.println("Scisors vs Paper, playerWin");
                            roundNumber=roundNumber+1;
                            playerWins=playerWins+1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);

                            break;
                    }

                }
                if(playerChoise.equalsIgnoreCase("Paper")) {
                    int computerChoise=rand.nextInt(3)+1;
                    switch(computerChoise) {
                        case 1:
                            System.out.println("Paper vs Rock, playerWin");
                            roundNumber=roundNumber+1;
                            playerWins=playerWins+1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);
                            break;

                        case 2:
                            System.out.println("Paper vs Scisors, computerWins");
                            roundNumber=roundNumber+1;
                            computerWins=computerWins+1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);

                            break;

                        case 3:
                            System.out.println("Paper vs Paper, Draw");
                            roundNumber=roundNumber+1;
                            Draw=Draw +1;
                            System.out.println("playerWins= "+ playerWins+ ", computerWins="+ computerWins+ ",Draw="+ Draw);
                            System.out.println("RoundNumber="+ roundNumber);

                            break;


                    }
                }
                Winner(playerWins, computerWins);
            }
        }





